import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.text.DecimalFormat;
import java.util.Scanner;
import java.lang.System;
/**

  Letter frequency attack
  @author tko

usage: java LFA textfile amntOfLetters
example: java LFA hauhau 3      // converts top 3 frequent letters in a file
example: java LFA hauhau 1      // converts only the most frequent letter in a file

 */
public class LFA {
        private static String fullMessage;
        public static int[] calculateFrequency(File f) {
                DecimalFormat df = new DecimalFormat("#.##");
                fullMessage = "";
                String partialMessage = "";
                int[] letterFrequency = new int[26];
                int sum = 0;
                try {
                        Scanner s = new Scanner(f);
                        while (s.hasNext()) {
                                char temp[] = s.nextLine().toUpperCase().toCharArray();
                                partialMessage = new String(temp);
                                fullMessage += partialMessage;
                                for (int i = 0; i < temp.length; ++i) {
                                        if (Character.isLetter(temp[i])) {
                                                ++letterFrequency[temp[i] - 65];
                                                ++sum;
                                        }
                                }
                        }
                } catch (IOException ioe) {
                        System.err.println(ioe.getMessage());
                }
                // uncomment the loop below to print the relative frequencies
                //for (int i = 0; i < letterFrequency.length; ++i) {
                        //System.out.println(((char) (i + 65)) + ": " + letterFrequency[i] + " occurrence(s), " + df.format(((double) letterFrequency[i] / sum) * 100.0) + " % of total");
                //}
                //System.out.println("Total: " + sum + " letters");
                return letterFrequency;
        }
        public static int[][] arrangeLetters(int[] a) {
                int[][] res = new int[a.length][2];
                int[][] temp = new int[a.length][2];
                int max = 0;
                for (int i = 0; i < a.length; ++i) {
                        res[i][0] = i + 65;     // uppercase ASCII values for letters
                        res[i][1] = a[i];       // frequency of letters
                }
                Arrays.sort(res, new Comparator<int[]>() {
                        public int compare(int[] o1, int[] o2) {
                                return Integer.valueOf(o1[1]).compareTo(Integer.valueOf(o2[1]));        // sort by frequency
                        }
                });
                int j = 0;
                for (int i = res.length - 1; i >= 0; --i) {
                        temp[j][0] = res[i][0];
                        temp[j][1] = res[i][1];
                        ++j;
                }
                System.arraycopy(temp, 0, res, 0, temp.length);
                return res;
        }
        public static void substituteLetters(int[][] a, String msg, int oL) {
                boolean changed = false;
                Scanner subC = new Scanner(System.in);
                for (int i = 0; i < a.length; ++i) {
                        //System.out.println(a[i][0]);
                }
                System.out.println("");
                System.out.println("MSG ENCIPHERED:");
                System.out.println("");
                System.out.println(msg);
                final char[] mostUsed = { 'E','T','A','O','I','N','S','H','R','D','L','U','C','M','F','Y','W','G','P','B','V','K','J','X','Q','Z' };    // letters in the order of most frequent - least frequent (in English language)
                char[] cipherFrequency = new char[oL];
                for (int i = 0; i < cipherFrequency.length; ++i) {
                        cipherFrequency[i] = (char) a[i][0];
                        //System.out.println("cipherFrequency[" + i + "]: " + cipherFrequency[i]);
                }
                char[] substitutedMsg = msg.toCharArray();
                System.out.println("");
                System.out.println("*********************");
                System.out.println("");
                System.out.println("CONVERSIONS:");
                System.out.println("");
                for (int i = 0; i < oL; ++i) {
                        System.out.println(cipherFrequency[i] + " -> " + mostUsed[i]);
                }
                System.out.println("");
                System.out.println("*********************");
                for (int i = 0; i < substitutedMsg.length; ++i) {
                        changed = false;
                        if (Character.isLetter(substitutedMsg[i]) && !changed) {
                                // this awful switch case was put here just to showcase how things should not be done
                                switch (substitutedMsg[i]) {
                                        case 'A':
                                                substitutedMsg[i] = 'S';
                                                changed = true;
                                                break;
                                        case 'B':
                                                substitutedMsg[i] = 'L';
                                                changed = true;
                                                break;
                                        case 'C':
                                                substitutedMsg[i] = 'I';
                                                changed = true;
                                                break;
                                        case 'D':
                                                substitutedMsg[i] = 'X';
                                                changed = true;
                                                break;
                                        case 'E':
                                                substitutedMsg[i] = 'R';
                                                changed = true;
                                                break;
                                        case 'F':
                                                substitutedMsg[i] = 'O';
                                                changed = true;
                                                break;
                                        case 'G':
                                                substitutedMsg[i] = 'Y';
                                                changed = true;
                                                break;
                                        case 'H':
                                                substitutedMsg[i] = 'F';
                                                changed = true;
                                                break;
                                        case 'I':
                                                substitutedMsg[i] = 'U';
                                                changed = true;
                                                break;
                                        case 'J':
                                                substitutedMsg[i] = 'T';
                                                changed = true;
                                                break;
                                        case 'K':
                                                substitutedMsg[i] = 'J';
                                                break;
                                        case 'L':
                                                substitutedMsg[i] = 'P';
                                                changed = true;
                                                break;
                                        case 'M':
                                                substitutedMsg[i] = 'C';
                                                changed = true;
                                                break;
                                        case 'N':
                                                substitutedMsg[i] = 'B';
                                                changed = true;
                                                break;
                                        case 'O':
                                                substitutedMsg[i] = 'N';
                                                changed = true;
                                                break;
                                        case 'P':
                                                substitutedMsg[i] = 'V';
                                                changed = true;
                                                break;
                                        case 'Q':
                                                substitutedMsg[i] = 'Z';
                                                changed = true;
                                                break;
                                        case 'R':
                                                substitutedMsg[i] = 'D';
                                                changed = true;
                                                break;
                                        case 'S':
                                                break;
                                        case 'T':
                                                substitutedMsg[i] = 'K';
                                                changed = true;
                                                break;
                                        case 'U':
                                                substitutedMsg[i] = 'A';
                                                changed = true;
                                                break;
                                        case 'V':
                                                substitutedMsg[i] = 'H';
                                                changed = true;
                                                break;
                                        case 'W':
                                                substitutedMsg[i] = 'E';
                                                changed = true;
                                                break;
                                        case 'X':
                                                substitutedMsg[i] = 'W';
                                                changed = true;
                                                break;
                                        case 'Y':
                                                substitutedMsg[i] = 'G';
                                                changed = true;
                                                break;
                                        case 'Z':
                                                substitutedMsg[i] = 'M';
                                                changed = true;
                                                break;
                                        default:
                                                break;
                                }
                                //for (int j = 0; j < cipherFrequency.length; ++j) {
                                //      if (substitutedMsg[i] == cipherFrequency[j] && !changed) {
                                //              substitutedMsg[i] = mostUsed[j];
                                //              changed = true;
                                //      }
                                //}
                        }
                }
                String deciphered = new String(substitutedMsg);
                System.out.println("");
                System.out.println("MSG DECIPHERED BELOW:");
                System.out.println("");
                System.out.println(deciphered);
        }
        public static void main(String[] args) {
                File f = new File(args[0]);
                int oL = Integer.parseInt(args[1]);
                int cF[] = calculateFrequency(f);
                for (int i = 0; i < cF.length; ++i) {
                        //System.out.println(cF[i]);
                }
                int cF0[][] = arrangeLetters(cF);
                substituteLetters(cF0, fullMessage, oL);
        }
}